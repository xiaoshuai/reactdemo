create table todos (
 id int primary key,
 content varchar(200),
 time timestamp,
 status int
);