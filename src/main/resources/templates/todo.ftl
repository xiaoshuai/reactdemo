<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link href="/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
    <link href="/css/index.css" rel="stylesheet" type="text/css"/>
    <script type="text/javascript" src="/js/lib/jquery.min.js"></script>
</head>
<body>

<div class="container">
    <div class="header clearfix">
        <h3 class="text-muted">Todo</h3>
    </div>

    <div class="jumbotron" id="todo-container">
    </div>

    <footer class="footer">
        <p>&copy; reactdemo 2015</p>
    </footer>

</div> <!-- /container -->

<script type="text/javascript" src="/js/build/index.js"></script>

</body>
</html>