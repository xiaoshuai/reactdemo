module.exports = {
    entry: {
        index: "./index.jsx",
        sayhello: "./sayhello.jsx"
    },
    output: {
        path: "../js/build",
        filename: "[name].js"
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /(node_modules|bower_components)/,
                loader: 'babel',
                query: {
                    presets: ['es2015','react'],
                    cacheDirectory: true
                }
            }
        ]
    }
}