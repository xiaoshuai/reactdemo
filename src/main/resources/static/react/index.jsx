var React = require('react');
var ReactDOM = require('react-dom');
var Todo = require('./components/Todo.jsx');

ReactDOM.render(<Todo/>, document.getElementById("todo-container"));