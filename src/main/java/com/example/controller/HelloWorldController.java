package com.example.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by xiaoshuai on 2016/1/4.
 * Say Hello
 */
@Controller
public class HelloWorldController {

    @RequestMapping("/sayhello")
    public String sayHello() {
        return "sayhello";
    }
}
