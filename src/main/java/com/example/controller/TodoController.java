package com.example.controller;

import java.sql.Timestamp;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.dao.TodoDao;
import com.example.entity.Todo;

@RestController
public class TodoController {

	@Resource
	private TodoDao todoDao;

	@RequestMapping(value = "/list", method = RequestMethod.GET)
	public ModelMap list() {
		List<Todo> todos = todoDao.queryAll();
		ModelMap mm = new ModelMap();
		mm.addAttribute("todos", todos);
		mm.addAttribute("status", "success");
		return mm;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public ModelMap add(@ModelAttribute("todo") Todo todo) {
		Integer queryMaxId = todoDao.queryMaxId();
		if (queryMaxId != null) {
			todo.setId(queryMaxId + 1);
		} else {
			todo.setId(1);
		}
		todo.setTime(new Timestamp(System.currentTimeMillis()));
		todo.setStatus(1);
		todoDao.add(todo);
		ModelMap mm = new ModelMap();
		mm.addAttribute("status", "success");
		return mm;
	}

	@RequestMapping(value = "/delete/{todoId}", method = RequestMethod.DELETE)
	public ModelMap delete(@PathVariable Integer todoId) {
		Todo todo = todoDao.query(todoId);
		if (todo != null) {
			todoDao.delete(todoId);
		}
		ModelMap mm = new ModelMap();
		mm.addAttribute("status", "success");
		return mm;
	}

	@RequestMapping(value = "/update", method = RequestMethod.PUT)
	public ModelMap update(@ModelAttribute("todo") Todo todo) {
		todoDao.update(todo);
		ModelMap mm = new ModelMap();
		mm.addAttribute("status", "success");
		return mm;
	}

}
