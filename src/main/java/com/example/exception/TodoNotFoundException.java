package com.example.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
class TodoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 3731663789709154157L;

	public TodoNotFoundException(String todoId) {
		super("could not find todo '" + todoId + "'.");
	}
}