package com.example.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.example.entity.Todo;

@Repository
public class TodoDao {

	@Resource
	private JdbcTemplate jdbcTemplate;

	public List<Todo> queryAll() {
		List<Todo> todoList = this.jdbcTemplate.query("select id,content,time,status from todos",
				new RowMapper<Todo>() {
					public Todo mapRow(ResultSet rs, int rowNum) throws SQLException {
						Todo actor = new Todo();
						actor.setId(rs.getInt("id"));
						actor.setStatus(rs.getInt("status"));
						actor.setContent(rs.getString("content"));
						actor.setTime(rs.getTimestamp("time"));
						return actor;
					}
				});
		return todoList;
	}

	public void add(Todo todo) {
		this.jdbcTemplate.update("insert into todos(id,content,time,status) values(?,?,?,?)",
				new Object[] { todo.getId(), todo.getContent(), todo.getTime(), todo.getStatus() });
	}

	public Integer queryMaxId() {
		Integer maxId = this.jdbcTemplate.queryForObject("select max(id) from todos", Integer.class);
		return maxId;
	}

	public Todo query(Integer todoId) {
		Todo todo = this.jdbcTemplate.queryForObject("select id,content,time,status from todos where id=?",
				new Object[] { todoId }, new RowMapper<Todo>() {
					public Todo mapRow(ResultSet rs, int rowNum) throws SQLException {
						Todo actor = new Todo();
						actor.setId(rs.getInt("id"));
						actor.setStatus(rs.getInt("status"));
						actor.setContent(rs.getString("content"));
						actor.setTime(rs.getTimestamp("time"));
						return actor;
					}
				});
		return todo;
	}

	public void delete(Integer todoId) {
		this.jdbcTemplate.update("delete from todos where id=?", new Object[] { todoId });

	}

	public void update(Todo todo) {
		this.jdbcTemplate.update("update todos set status=? where id=?",
				new Object[] { todo.getStatus(), todo.getId() });
	}

}
