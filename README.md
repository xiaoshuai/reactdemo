# React Demo#

## Use Babel-loader ##
use code `npm install babel-loader babel-core babel-preset-es2015 babel-preset-react webpack --save-dev` to install modules.

```
    loaders: [
        {
            test: /\.jsx?$/,
            exclude: /(node_modules|bower_components)/,
            loader: 'babel',
            query: {
                presets: ['es2015','react'],
                cacheDirectory: true
            }
        }
    ]
```


## Learn video ##
Reference: [使用Flask和React开发Todo 应用](http://www.jikexueyuan.com/course/1822.html)